// SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    objectName: "defaultCover"

    CoverPlaceholder {
        objectName: "placeholder"
        text: qsTr("Geolocation")
        icon {
            source: Qt.resolvedUrl("../images/Geolocation.svg")
            sourceSize { 
                width: icon.width; 
                height: icon.height 
            }
        }
        forceFit: true
    }
}
