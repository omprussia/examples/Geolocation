// SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import QtSensors 5.2
import QtLocation 5.0
import QtPositioning 5.2
import Sailfish.Silica 1.0
import ru.auroraos.Geolocation 1.0

Page {
    objectName: "mapPage"

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: appWindow.appName
        extraContent.children: [
            IconButton {
                objectName: "pageHeaderButton"
                anchors.verticalCenter: parent.verticalCenter
                icon {
                    source: "image://theme/icon-m-about"
                    sourceSize {
                        width: Theme.iconSizeMedium
                        height: Theme.iconSizeMedium
                    }
                }

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Compass {
        id: compass

        objectName: "compass"
        active: true
    }

    GpsInfoProvider {
        id: gpsInfoProvider

        objectName: "gpsInfoProvider"
        active: true
    }

    Drawer {
        id: drawer

        objectName: "drawer"
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        open: false
        backgroundSize: backgroundView.contentHeight
        background: SilicaListView {
            id: backgroundView

            objectName: "backgroundView"
            anchors.fill: parent
            model: [
                {
                    labelText: qsTr("Satellite map"),
                    destinationPage: "SatelliteInfoPage",
                    destinationProperties: { "compass": compass, "gpsInfoProvider": gpsInfoProvider }
                }, {
                    labelText: qsTr("Location information"),
                    destinationPage: "LocationInfoPage",
                    destinationProperties: { "gpsInfoProvider": gpsInfoProvider }
                }
            ]
            delegate: BackgroundItem {
                id: drawerMenuItem

                objectName: "drawerMenuItem_%1".arg(index)
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: contentHeight
                contentHeight: Theme.itemSizeMedium

                onClicked: {
                    drawer.hide();
                    pageStack.push(Qt.resolvedUrl("%1.qml".arg(modelData.destinationPage)),
                                   modelData.destinationProperties);
                }

                Label {
                    objectName: "drawerItemLabel"
                    anchors.centerIn: parent
                    text: modelData.labelText
                }
            }
        }

        Rectangle {
            objectName: "mapRectangle"
            anchors.fill: parent

            Plugin {
                id: mapPlugin

                objectName: "mapPlugin"
                name: "webtiles"
                allowExperimental: false

                PluginParameter {
                    objectName: "schemeParameter"
                    name: "webtiles.scheme"
                    value: "https"
                }

                PluginParameter {
                    objectName: "hostParameter"
                    name: "webtiles.host"
                    value: "tile.openstreetmap.org"
                }

                PluginParameter {
                    objectName: "pathParameter"
                    name: "webtiles.path"
                    value: "/${z}/${x}/${y}.png"
                }
            }

            Map {
                id: map

                objectName: "map"
                anchors.fill: parent
                plugin: mapPlugin
                minimumZoomLevel: 2.5
                maximumZoomLevel: 19.0
                zoomLevel: minimumZoomLevel
                center: gpsInfoProvider.coordinate

                MapQuickItem {
                    objectName: "positionItem"
                    rotation: compass.connectedToBackend ? compass.reading.azimuth - 90 : 0
                    coordinate: positionCircle.center
                    sourceItem: Image {
                        id: image

                        source: (compass.connectedToBackend ? "image://theme/icon-m-send?%1"
                                                            : "image://theme/icon-m-location?%1")
                                .arg(positionCircle.border.color)
                    }
                    anchorPoint {
                        x: image.width / 2
                        y: compass.connectedToBackend ? (image.height / 2) : image.height
                    }
                }

                MapCircle {
                    id: positionCircle

                    objectName: "positionCircle"
                    color: Theme.highlightColor
                    radius: gpsInfoProvider.horizontalAccuracy
                    center: gpsInfoProvider.coordinate
                    border.color: Qt.darker(color)
                    opacity: 0.5
                }
            }

            Button {
                objectName: "menuButton"
                anchors {
                    left: parent.left
                    bottom: parent.bottom
                    margins: Theme.paddingLarge
                }
                width: height
                color: Theme.highlightDimmerColor
                border {
                    color: Theme.rgba(color, Theme.opacityFaint)
                    highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                }
                icon.source: "image://theme/icon-m-menu?%1".arg(Theme.highlightDimmerColor)

                onClicked: {
                    if (drawer.opened) {
                        drawer.hide();
                    } else {
                        drawer.show();
                    }
                }
            }

            Button {
                objectName: "centerButton"
                anchors {
                    right: parent.right
                    bottom: parent.bottom
                    margins: Theme.paddingLarge
                }
                width: height
                color: Theme.highlightDimmerColor
                border {
                    color: Theme.rgba(color, Theme.opacityFaint)
                    highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                }
                icon.source: "image://theme/icon-m-whereami?%1".arg(Theme.highlightDimmerColor)

                onClicked: {
                    map.zoomLevel = map.maximumZoomLevel;
                    map.center.latitude = 0;
                    map.center.longitude = 0;
                    map.center = positionCircle.center;
                }
            }
        }

        MouseArea {
            objectName: "closeDrawerArea"
            anchors.fill: parent
            enabled: drawer.opened

            onClicked: drawer.open = !drawer.open
        }
    }
}
