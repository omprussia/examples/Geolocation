// SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import QtSensors 5.2
import Sailfish.Silica 1.0
import ru.auroraos.Geolocation 1.0

Page {
    property Compass compass
    property GpsInfoProvider gpsInfoProvider

    objectName: "satelliteInfoPage"

    SilicaFlickable {
        objectName: "satelliteContentFlickable"
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }

            PageHeader {
                objectName: "pageHeader"
                title: qsTr("Satellites information")
            }

            Canvas {
                property int compassNorth: compass.connectedToBackend ? compass.reading.azimuth : 0
                property var satellites: gpsInfoProvider.satellites

                function drawBackground(context, radius, center) {
                    var gradient = context.createRadialGradient(center.x, center.y, 5,
                                                                center.x, center.y, radius);
                    gradient.addColorStop(0,"rgba(255,255,0,0.3)");
                    gradient.addColorStop(1,"rgba(255,255,0,0.6)");
                    context.fillStyle = gradient;
                    context.beginPath();
                    context.arc(center.x, center.y, radius, 0, Math.PI * 2, false);
                    context.closePath();
                    context.fill();
                }

                function drawCompassLines(context, radius, center, compassPosition) {
                    context.strokeStyle = "rgb(255,255,0)";
                    context.beginPath();
                    context.moveTo(compassPosition.north.x, compassPosition.north.y);
                    context.lineTo(compassPosition.south.x, compassPosition.south.y);
                    context.closePath();
                    context.stroke();

                    context.beginPath();
                    context.moveTo(compassPosition.west.x, compassPosition.west.y);
                    context.lineTo(compassPosition.east.x, compassPosition.east.y);
                    context.closePath();
                    context.stroke();

                    context.beginPath();
                    context.arc(center.x, center.y, radius * Math.cos(Math.PI * 1 / 6), 0, Math.PI * 2, false);
                    context.closePath();
                    context.stroke();

                    context.beginPath();
                    context.arc(center.x, center.y, radius * Math.cos(Math.PI * 2 / 6), 0, Math.PI * 2, false);
                    context.closePath();
                    context.stroke();
                }

                function drawDirectionSigns(context, compassPosition) {
                    context.textAlign = "center";
                    context.font = Theme.fontSizeSmall + "px " + Theme.fontFamily;
                    var rectangleSize = Theme.fontSizeSmall + 3;

                    context.fillStyle = "rgb(125,0,255)";
                    context.fillRect(compassPosition.north.x - rectangleSize / 2,
                                     compassPosition.north.y - rectangleSize / 2,
                                     rectangleSize, rectangleSize);
                    context.fillRect(compassPosition.south.x - rectangleSize / 2,
                                     compassPosition.south.y - rectangleSize / 2,
                                     rectangleSize, rectangleSize);
                    context.fillRect(compassPosition.west.x - rectangleSize / 2,
                                     compassPosition.west.y - rectangleSize / 2,
                                     rectangleSize, rectangleSize);
                    context.fillRect(compassPosition.east.x - rectangleSize / 2,
                                     compassPosition.east.y - rectangleSize / 2,
                                     rectangleSize, rectangleSize);

                    context.fillStyle = "rgb(255,255,255)"
                    context.fillText("N", compassPosition.north.x,
                                     compassPosition.north.y + Theme.fontSizeSmall / 2 - 5);
                    context.fillText("S", compassPosition.south.x,
                                     compassPosition.south.y + Theme.fontSizeSmall / 2 - 5);
                    context.fillText("W", compassPosition.west.x,
                                     compassPosition.west.y + Theme.fontSizeSmall / 2 - 5);
                    context.fillText("E", compassPosition.east.x,
                                     compassPosition.east.y + Theme.fontSizeSmall / 2 - 5);
                }

                function drawSatellites(context, radius, center) {
                    var signSizeInactive = Theme.fontSizeExtraSmall + 4;
                    var signSizeActive = Theme.fontSizeExtraSmall + 4;
                    satellites.forEach(function(satellite) {
                        var azimuthRad = ((satellite.azimuth - compassNorth) % 360) * (Math.PI / 180);
                        var elevationRad = satellite.elevation * (Math.PI / 180);
                        var x = center.x + Math.sin(azimuthRad) * radius * Math.cos(elevationRad);
                        var y = center.y - Math.cos(azimuthRad) * radius * Math.cos(elevationRad);

                        var hue = (satellite.signalStrength < 40 ? satellite.signalStrength : 40) * 3;
                        if (satellite.inUse) {
                            context.fillStyle = "rgb(255,255,255)";
                            context.fillRect(x - signSizeActive / 2 - 2, y - signSizeActive / 2 - 2,
                                             signSizeActive + 4, signSizeActive + 4);
                            context.fillStyle = "hsl(" + hue + ",100%,35%)";
                            context.fillRect(x - signSizeActive / 2, y - signSizeActive / 2,
                                             signSizeActive, signSizeActive);
                            context.fillStyle = "rgb(255,255,255)";
                            context.font = Theme.fontSizeExtraSmall + "px " + Theme.fontFamily;
                            context.fillText(satellite.identifier, x, y + Theme.fontSizeExtraSmall / 2 - 5);
                        } else {
                            context.fillStyle = "hsl(" + hue + ",100%,35%)";
                            context.fillRect(x - signSizeInactive / 2, y - signSizeInactive / 2,
                                             signSizeInactive, signSizeInactive);
                            context.fillStyle = "rgb(255,255,255)";
                            context.font = Theme.fontSizeExtraSmall + "px " + Theme.fontFamily;
                            context.fillText(satellite.identifier, x, y + Theme.fontSizeExtraSmall / 2 - 5);
                        }
                    })
                }

                objectName: "satelliteCanvas"
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: width

                onCompassNorthChanged: requestPaint()
                onSatellitesChanged: requestPaint()
                onPaint: {
                    if (!visible) {
                        return;
                    }

                    var context = getContext('2d');
                    context.clearRect(0, 0, width, width);

                    var radius = width / 2 - Theme.horizontalPageMargin;
                    var center = { x: width / 2, y: width / 2 };
                    var northRadians = compassNorth * (Math.PI / 180);
                    var compassPosition = {
                        north: {
                            x: center.x - Math.sin(northRadians) * radius,
                            y: center.y - Math.cos(northRadians) * radius
                        },
                        south: {
                            x: center.x + Math.sin(northRadians) * radius,
                            y: center.y + Math.cos(northRadians) * radius
                        },
                        west: {
                            x: center.x + Math.sin(northRadians - Math.PI / 2) * radius,
                            y: center.y + Math.cos(northRadians - Math.PI / 2) * radius
                        },
                        east: {
                            x: center.x + Math.sin(northRadians + Math.PI / 2) * radius,
                            y: center.y + Math.cos(northRadians + Math.PI / 2) * radius
                        }
                    };
                    drawBackground(context, radius, center);
                    drawCompassLines(context, radius, center, compassPosition);
                    drawDirectionSigns(context, compassPosition);
                    drawSatellites(context, radius, center);
                }
            }

            Label {
                objectName: "satellitesAvailableLabel"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Satellites available: %1").arg(gpsInfoProvider.satellitesInViewCount)
            }

            Label {
                objectName: "satInUseLabel"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Satellites in use: %1").arg(gpsInfoProvider.satellitesInUseCount)
            }

            SilicaListView {
                objectName: "satellitesListView"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: contentHeight
                header: Row {
                    objectName: "satellitesListHeader"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    spacing: Theme.paddingLarge

                    Label {
                        objectName: "identifierHeaderLabel"
                        text: qsTr("Identifier")
                        width: (parent.width - parent.spacing) / 2
                        horizontalAlignment: Text.AlignHCenter
                    }

                    Label {
                        objectName: "signalHeaderLabel"
                        text: qsTr("Signal strength")
                        width: (parent.width - parent.spacing) / 2
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
                model: gpsInfoProvider.satellites.sort(function (satA, satB) {
                    return satB.signalStrength - satA.signalStrength
                })
                delegate: Row {
                    objectName: "satellitesListRow_%1".arg(index)
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    spacing: Theme.paddingLarge

                    Label {
                        objectName: "identifierLabel"
                        text: modelData.identifier
                        width: (parent.width - parent.spacing) / 2
                        horizontalAlignment: Text.AlignHCenter
                    }

                    Label {
                        objectName: "signalLabel"
                        text: modelData.signalStrength
                        width: (parent.width - parent.spacing) / 2
                        horizontalAlignment: Text.AlignHCenter
                    }
                }
            }
        }
    }
}
