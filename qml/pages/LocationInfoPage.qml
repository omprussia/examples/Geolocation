// SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import QtLocation 5.0
import QtPositioning 5.2
import Sailfish.Silica 1.0
import ru.auroraos.Geolocation 1.0

Page {
    property GpsInfoProvider gpsInfoProvider

    objectName: "locationInfoPage"

    SilicaListView {
        objectName: "listView"
        anchors.fill: parent
        spacing: Theme.paddingLarge
        header: PageHeader {
            objectName: "pageHeader"
            title: qsTr("Location information")
        }
        model: [{
                "name": "GPS",
                "value": gpsInfoProvider.gpsEnabled ? qsTr("On") : qsTr("Off")
            }, {
                "name": qsTr("Position source"),
                "value": gpsInfoProvider.positioningMethod
            }, {
                "name": qsTr("Latitude"),
                "value": gpsInfoProvider.coordinate.latitude
            }, {
                "name": qsTr("Longitude"),
                "value": gpsInfoProvider.coordinate.longitude
            }, {
                "name": qsTr("Altitude"),
                "value": gpsInfoProvider.coordinate.altitude
            }, {
                "name": qsTr("Vertical accuracy"),
                "value": gpsInfoProvider.verticalAccuracy >= 0 ? gpsInfoProvider.verticalAccuracy : "-"
            }, {
                "name": qsTr("Horizontal accuracy"),
                "value": gpsInfoProvider.horizontalAccuracy >= 0 ? gpsInfoProvider.horizontalAccuracy : "-"
            }, {
                "name": qsTr("Speed"),
                "value": gpsInfoProvider.speed >= 0 ? "%1 %2 (%3)"
                                                      .arg(gpsInfoProvider.speed).arg(qsTr("m/s"))
                                                      .arg(gpsInfoProvider.timestamp.toLocaleTimeString())
                                                    : "-"
            }, {
                "name": qsTr("Direction"),
                "value": gpsInfoProvider.direction
            }]
        delegate: Row {
            objectName: "infoRow_%1".arg(index)
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingLarge

            Label {
                objectName: "nameLabel"
                text: modelData.name
                width: (parent.width - parent.spacing) / 2
                horizontalAlignment: Text.AlignRight
                wrapMode: Text.Wrap
            }

            Label {
                objectName: "valueLabel"
                text: modelData.value || "-"
                width: (parent.width - parent.spacing) / 2
                wrapMode: Text.Wrap
            }
        }
    }
}
