// SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtPositioning/QGeoPositionInfo>
#include <auroraapp.h>

#include "gpsinfoprovider/gpsinfoprovider.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<GpsInfoProvider>("ru.auroraos.Geolocation", 1, 0, "GpsInfoProvider");

    qRegisterMetaType<QGeoPositionInfo>("QGeoPositionInfo");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("Geolocation"));

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/Geolocation.qml")));
    view->show();

    return application->exec();
}
