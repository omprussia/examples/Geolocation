<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Проект предоставляет примеры использования API, который позволяет работать с
                            геолокацией: позиционирование, спутниковая информация, карты.&lt;/p&gt;
                            &lt;p&gt;Основная цель - показать не только, какие функции доступны для
                            работы с этими API, но и как их правильно использовать.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2023 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Geolocation</source>
        <translation>Геолокация</translation>
    </message>
</context>
<context>
    <name>Geolocation</name>
    <message>
        <location filename="../qml/Geolocation.qml" line="11"/>
        <source>Geolocation</source>
        <translation>Геолокация</translation>
    </message>
</context>
<context>
    <name>GpsInfoProvider</name>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="89"/>
        <source>North</source>
        <translation>Север</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="91"/>
        <source>Northeast</source>
        <translation>Северо-восток</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="93"/>
        <source>East</source>
        <translation>Восток</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="95"/>
        <source>Southeast</source>
        <translation>Юго-восток</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="97"/>
        <source>South</source>
        <translation>Юг</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="99"/>
        <source>Southwest</source>
        <translation>Юго-запад</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="101"/>
        <source>West</source>
        <translation>Запад</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="103"/>
        <source>Northwest</source>
        <translation>Северо-запад</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="129"/>
        <source>Satellite and network</source>
        <translation>Спутник и сеть</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="131"/>
        <source>Network</source>
        <translation>Сеть</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="133"/>
        <source>Satellite</source>
        <translation>Спутник</translation>
    </message>
</context>
<context>
    <name>LocationInfoPage</name>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="21"/>
        <source>Location information</source>
        <translation>Информация о геопозиции</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="25"/>
        <source>On</source>
        <translation>Включен</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="25"/>
        <source>Off</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="27"/>
        <source>Position source</source>
        <translation>Источник позиционирования</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="30"/>
        <source>Latitude</source>
        <translation>Широта</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="33"/>
        <source>Longitude</source>
        <translation>Долгота</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="36"/>
        <source>Altitude</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="39"/>
        <source>Vertical accuracy</source>
        <translation>Вертикальная точность</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="42"/>
        <source>Horizontal accuracy</source>
        <translation>Горизонтальная точность</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="45"/>
        <source>Speed</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="47"/>
        <source>m/s</source>
        <translation>м/с</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="51"/>
        <source>Direction</source>
        <translation>Направление</translation>
    </message>
</context>
<context>
    <name>MapPage</name>
    <message>
        <location filename="../qml/pages/MapPage.qml" line="69"/>
        <source>Satellite map</source>
        <translation>Карта спутников</translation>
    </message>
    <message>
        <location filename="../qml/pages/MapPage.qml" line="73"/>
        <source>Location information</source>
        <translation>Информация о геопозиции</translation>
    </message>
</context>
<context>
    <name>SatelliteInfoPage</name>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="31"/>
        <source>Satellites information</source>
        <translation>Информация о спутниках</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="190"/>
        <source>Satellites available: %1</source>
        <translation>Спутников доступно: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="200"/>
        <source>Satellites in use: %1</source>
        <translation>Спутников используется: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="220"/>
        <source>Identifier</source>
        <translation>Идентификатор</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="227"/>
        <source>Signal strength</source>
        <translation>Сила сигнала</translation>
    </message>
</context>
</TS>
