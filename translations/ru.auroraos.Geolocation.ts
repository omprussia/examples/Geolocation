<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;The project provides the usage examples of the API that allows to work
                            with geolocation: positioning, satellites info, maps.&lt;/p&gt;
                            &lt;p&gt;The main purpose is to show not only what features are available to
                            work with these API, but also how to use them correctly.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2023 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Geolocation</source>
        <translation>Geolocation</translation>
    </message>
</context>
<context>
    <name>Geolocation</name>
    <message>
        <location filename="../qml/Geolocation.qml" line="11"/>
        <source>Geolocation</source>
        <translation>Geolocation</translation>
    </message>
</context>
<context>
    <name>GpsInfoProvider</name>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="89"/>
        <source>North</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="91"/>
        <source>Northeast</source>
        <translation>Northeast</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="93"/>
        <source>East</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="95"/>
        <source>Southeast</source>
        <translation>Southeast</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="97"/>
        <source>South</source>
        <translation>South</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="99"/>
        <source>Southwest</source>
        <translation>Southwest</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="101"/>
        <source>West</source>
        <translation>West</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="103"/>
        <source>Northwest</source>
        <translation>Northwest</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="129"/>
        <source>Satellite and network</source>
        <translation>Satellite and network</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="131"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../src/gpsinfoprovider/gpsinfoprovider.cpp" line="133"/>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
</context>
<context>
    <name>LocationInfoPage</name>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="21"/>
        <source>Location information</source>
        <translation>Location information</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="25"/>
        <source>On</source>
        <translation>On</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="25"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="27"/>
        <source>Position source</source>
        <translation>Position source</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="30"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="33"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="36"/>
        <source>Altitude</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="39"/>
        <source>Vertical accuracy</source>
        <translation>Vertical accuracy</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="42"/>
        <source>Horizontal accuracy</source>
        <translation>Horizontal accuracy</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="45"/>
        <source>Speed</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="47"/>
        <source>m/s</source>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../qml/pages/LocationInfoPage.qml" line="51"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
</context>
<context>
    <name>MapPage</name>
    <message>
        <location filename="../qml/pages/MapPage.qml" line="69"/>
        <source>Satellite map</source>
        <translation>Satellite map</translation>
    </message>
    <message>
        <location filename="../qml/pages/MapPage.qml" line="73"/>
        <source>Location information</source>
        <translation>Location information</translation>
    </message>
</context>
<context>
    <name>SatelliteInfoPage</name>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="31"/>
        <source>Satellites information</source>
        <translation>Satellites information</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="190"/>
        <source>Satellites available: %1</source>
        <translation>Satellites available: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="200"/>
        <source>Satellites in use: %1</source>
        <translation>Satellites in use: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="220"/>
        <source>Identifier</source>
        <translation>Identifier</translation>
    </message>
    <message>
        <location filename="../qml/pages/SatelliteInfoPage.qml" line="227"/>
        <source>Signal strength</source>
        <translation>Signal strength</translation>
    </message>
</context>
</TS>
