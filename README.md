# Geolocation

The project provides the examples of using an API that allows to work with geolocation:
positioning, satellites info, maps.

The main purpose is to show not only what features are available to work with these API,
but also how to use them correctly.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.Geolocation.pro](ru.auroraos.Geolocation.pro)** file describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[images](qml/images)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[Geolocation.qml](qml/Geolocation.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.Geolocation.spec](rpm/ru.auroraos.Geolocation.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[satelliteinfo](src/satelliteinfo)** directory contains information about available satellites.
  * **[gpsinfoprovider](src/gpsinfoprovider)** directory contains an interface for data management.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.Geolocation.desktop](ru.auroraos.Geolocation.desktop)** file defines the display and parameters for launching the application.

## Compatibility

The project is compatible with all the current versions of the Aurora OS.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
