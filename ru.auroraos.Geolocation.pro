# SPDX-FileCopyrightText: 2021-2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.Geolocation

QT += network sensors positioning location

CONFIG += \
    auroraapp \
    auroraapp_i18n \

TRANSLATIONS += \
    translations/ru.auroraos.Geolocation.ts \
    translations/ru.auroraos.Geolocation-ru.ts \

HEADERS += \
    src/gpsinfoprovider/gpsinfoprovider.h \
    src/satelliteinfo/satelliteinfo.h \

SOURCES += \
    src/gpsinfoprovider/gpsinfoprovider.cpp \
    src/satelliteinfo/satelliteinfo.cpp \
    src/main.cpp \

DISTFILES += \
    rpm/ru.auroraos.Geolocation.spec \
    LICENSE.BSD-3-Clause.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    AUTHORS.md \
    README.md \
    README.ru.md \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
